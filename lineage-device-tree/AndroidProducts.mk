#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_kaiser.mk

COMMON_LUNCH_CHOICES := \
    lineage_kaiser-user \
    lineage_kaiser-userdebug \
    lineage_kaiser-eng
